# Тестовое задание для кандидата на вакансию back‐end разработчика:
 
Предлагаем Вам продемонстрировать компании Snab2b Ваши знания и выполнить тестовое задание.

Необходимо добавить функционал корзины заказов интернет-магазина на базе демонстрационного приложения на [Phalcon PHP](https://phalconphp.com/):

* Поддержка пользователей двух ролей: администратор (роль user - уже присутствует) и клиент (роль customer - необходимо добавить)
* Отображение на главной странице список товаров магазина по категориям (витрина)
* Залогиненый клиент может добавить любой товар к себе в корзину
* Клиент может удалять позиции из корзины и изменять количество приобретаемого товара
* Клиент может "приобрести" товары (заказ клиента должен отображаться во вкладке invoices у администратора) 
* Отображение в корзине итоговой суммы заказа
* Администратор магазина может просматривать заявки оставленные клиентами магазина (invo.local/invoices)

На вёрстку внимание обращаться не будет, важно оформление кода, phpdoc, использование фич php7+ и возможностей фреймворка.
 
Ожидаем от Вас pull-запрос с выполненым заданием в [этот репозиторий](https://bitbucket.org/snab2b/snab2b_backend_quiz/src).

Если у Вас есть желание продемонстрировать знание какой-то технологии или подхода, то можно
реализовать произвольную дополнительную функциональность на Ваше усмотрение.

# INVO Application

[Phalcon PHP](https://phalconphp.com/) is a web framework delivered as a C extension providing high
performance and lower resource consumption.

This is a sample application for the Phalcon PHP Framework. We expect to
implement as many features as possible to showcase the framework and its
potential.

Please write us if you have any feedback.

Thanks.

## Demo App

https://docs.phalconphp.com/3.4/ru-ru/tutorial-invo

## Get Started

### Requirements

To run this stack on your machine, you need at least:

- Operating System: Windows, Linux, or OS X
- [Docker Engine](https://docs.docker.com/installation/) >= 1.10.0
- [Docker Compose](https://docs.docker.com/compose/install/) >= 1.6.2

### Usage

Add your Phalcon Application into application folder.

Configuration

Add invo.local (or your preferred host name) in your /etc/hosts file as follows:

127.0.0.1 www.invo.local invo.local

Usage

You can now build, create, start, and attach to containers to the environment for your application. To build the containers use following command inside the project root:

```
docker-compose build
```
To start the application and run the containers in the background, use following command inside project root:
```
$ docker-compose up -d
```
Now setup your project in the app container using the Phalcon Developer Tools

More info about Docker https://docs.phalconphp.com/3.4/ru-ru/environments-docker
More info about Vagrant  https://docs.phalconphp.com/3.4/ru-ru/environments-vagrant
