
{{ content() }}

<div align="right">
    {{ link_to("companies/new", "Create Companies", "class": "btn btn-primary") }}
</div>

{{ form("companies/search", 'method': 'post') }}

<h2>Search companies</h2>

<fieldset>

{% for element in form %}
    {% if is_a(element, 'Phalcon\Forms\Element\Hidden') %}
{{ element }}
    {% else %}
<div class="control-group">
    {{ element.label(['class': 'control-label']) }}
    <div class="controls">
        {{ element }}
    </div>
</div>
    {% endif %}
{% endfor %}

<div class="control-group">
    {#{{ submit('Submit') }}#}
    <input type="submit" value="Search" class="btn btn-success">
</div>

</fieldset>

{{ end_form() }}
